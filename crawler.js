require('dotenv').load();

const Crawler = require("crawler")
const Minio = require('minio')
const Couchbase = require('couchbase')
const request = require('request')
const uuidv4 = require('uuid/v4')

const LIMIT = 5
let counter = 0
let itemSaved = 0
const DOMAIN = "http://www2.hm.com/en_gb/"
const START_PAGE = DOMAIN + "productpage.0241602003.html"

// "http://www2.hm.com/en_gb/productpage.0584928010.html" // Woman jacket
// "http://www2.hm.com/en_gb/productpage.0241602003.html" // Man blazer
// "http://www2.hm.com/en_gb/productpage.0683527001.html" // Kid (girl) hoodie
// "http://www2.hm.com/fr_ch/productpage.0693208008.html" // Kid (boy) T-shirt
// "http://www2.hm.com/fr_ch/productpage.0644630003.html" // Baby outfit

const SEASONS = ['spring-summer', 'fall-winter']

function getGender(cat) {
  switch(cat) {
    case "man":
    case "men":
    case "boy":
    case "boys":
      return 'M'
    case "lady":
    case "ladies":
    case "girl":
    case "girls":
      return 'F'
    default:
      return "?"
  }
}

function getAgeCat(cat) {
  switch(cat) {
    case "newborn":
    case "newborns":
      return "newborn"
    case "kid":
    case "kids":
    case "boy":
    case "boys":
    case "girl":
    case "girls":
      return "kids"
    case "men": 
    case "ladies":
      return "adults"
    default:
      return "?"
  }
}

function JSONSanitizer(brokenJSON) {
  return brokenJSON.replace(/\d'\d"\s/g, (retardUnit) => retardUnit
    .replace(/'/g, "%27")
    .replace(/"/g, "%22")
  )
  .replace(/'/g, "\"")
  .replace(/\(/g, "%28")
  .replace(/\)/g, "%29")
  .replace(/(\"\+\")|(isDesktop \? )|(fullscreen\]\"\ : \".*?mobile)|(main\]\" \: \".*?mobile)|(=zoom\" \: \".*?zoom=)|(\/\/www2\.hm\.com\/\" \: \")/g, "")
}

const minioClient = new Minio.Client({
  endPoint: process.env.MINIO_ADDRESS,
  port: parseInt(process.env.MINIO_PORT),
  useSSL: false,
  accessKey: process.env.MINIO_USER,
  secretKey: process.env.MINIO_KEY
});

const cluster = new Couchbase.Cluster('couchbase://intra.curty.tech/')
cluster.authenticate(process.env.CB_USER, process.env.CB_PASS)
const bucket = cluster.openBucket('articles')

const crawler = new Crawler({
  rateLimit: 1000,
  // This will be called for each crawled page
  callback : function (error, response, done) {
    if(!error && response.body){
      const $ = response.$
      const today = new Date()

      let rawData = null;
      let strCatAge = "";

      console.log("Start:", response.request.href)
      
      ///////////////////
      // Get usefull data
      ///////////////////
      const data = JSON.parse(JSONSanitizer($("script[type='application/ld+json']").first().html()))

      $("script").each((i, el) => {
        el = $(el).first()
        if(Object.keys(el.attr()).length === 0) {
          const find = el.html().match(/var productArticleDetails = /)
          if(!!find) {
            rawData = find.input.split("var productArticleDetails =")[1].trim().slice(0,-1).replace(/\r?\n|\r|(\s{2,})/g, "")
            return
          }
        }
      })

      ///////////////////
      // Data treatment
      ///////////////////
      const JSONData = JSON.parse(JSONSanitizer(rawData))

      let urlData = (!!data.image) ? data.image.match(/category\[.*?\]/) : ""

      // Save Images
      const imgTreatment = []
      const imagesList = JSONData[data.sku].images
      for (let i = 0; i<imagesList.length; i++) {
        const imgUrl = imagesList[i].image

        // Get gender / age data from image URL
        if(!strCatAge) {
          if(!!urlData && urlData.length > 0)
            strCatAge = urlData[0].split("[")[1].split(/(\dy)|(\_)/)
          else {
            urlData = imgUrl.match(/category\[.*?\]/)
            if(!!urlData && urlData.length > 0)
              strCatAge = urlData[0].split("[")[1].split(/(\dy)|(\_)/)
          }
        }
        
        // Get images URLs
        if(/type\[DESCRIPTIVE/.test(imgUrl) || /type\[DETAIL/.test(imgUrl) || /type\[STILLLIFE/.test(imgUrl)){
          const imgBrand = "H_M"
          const imgSource = imgUrl.match(/source\[.*?\]/)[0].split(/\/|\]/)
          const imgName = imgBrand + "-" + imgSource[imgSource.length-2]

          const options = {
            url: "http:" + imgUrl,
            headers: {
              'User-Agent': 'Mozilla/4.0 (compatible; MSIE 6.1; Windows XP)'
            },
            encoding: null
          }
          const promiseImg = new Promise((resolve, reject) => {
            request.get(options, (error, response, body) => {
              if (!error && response.statusCode == 200)
                minioClient.putObject("armarium", imgName, body, (e) => {
                  if (e) reject(e)
                  else resolve({
                    name: encodeURI(imgName),
                    path: encodeURI("http://" + process.env.MINIO_ADDRESS + ":" + process.env.MINIO_PORT + "/armarium/" + imgName),
                    source: imgUrl
                  })
                })
              else reject(error)
            })
          })

          imgTreatment.push(promiseImg)
        }
      }

      Promise.all(imgTreatment).then((values) => {
        const medias = values
        const words = JSONData[data.sku].description.split(".")[0].split(" ").map((w) => w.replace(/[^\w\s]|_/g, " ").trim()).filter((w) => w.length > 3).concat([
          data.sku,
          data.brand.name.replace("&amp;", "&"),
        ])
        const links = []
        for (k in JSONData[data.sku])
          if(JSONData[data.sku][k] == 'manual' && /\d+/.test(k))
            links.push(DOMAIN + "productpage." + k + ".html")

        // Texts
        const item = {
          name: $('title').first().html().replace("&amp;", "&"),
          infos: {
            brand: data.brand.name.replace("&amp;", "&"),
            type: data.category.name,
            color: JSONData[data.sku].name,
            genders: (!!strCatAge) ? getGender((strCatAge[0] == 'kids') ? strCatAge[3] : strCatAge[0]) : "?",
            age_groups: (!!strCatAge) ? getAgeCat((strCatAge[3] == 'newborn') ? strCatAge[3] : strCatAge[0]) : "?",
            sizes: JSONData[data.sku].sizes.map((e) => e.name),
            keywords: words
          },
          season: ((today.getMonth() >= 3 && today.getMonth() <= 8) ? SEASONS[0] : SEASONS[1]) + " " + today.getFullYear(),
          source: {
            url: response.request.href,
            infos: {
              id: data.sku,
              price: JSONData[data.sku].whitePrice,
              appeared_at: ('0' + (today.getMonth()+1)).slice(-2) + "." + today.getFullYear()
            },
          },
          media: medias,
          created_at: today.toISOString()
        }

        console.log(item)

        ///////////////////
        // Data persistence
        ///////////////////

        // TODO: test if article already exists
        
        const id = uuidv4()
        bucket.upsert(id, item, function(err, result) {
          if (err) throw err;
        
          bucket.get(id, function(err, result) {
            if (err) throw err;

            if(counter < LIMIT){
              for(l of links)
                crawler.queue(l)
              counter += 1
            }
            itemSaved +=1

            console.log(`Item saved (${counter} depths):`, itemSaved)
            console.log("-----")
            done()
          });
        })
      })
      .catch(reason => { 
        console.log("Images treatment error.", reason)
      })

    }
    else console.log("Empty body or error.", error)
  }
})

// Start crawling
crawler.queue(START_PAGE)

crawler.on('drain', function(){
  console.log("Done, without error.")
  process.exit(0);
});